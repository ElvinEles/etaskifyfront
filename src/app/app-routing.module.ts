import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { RegisterComponent } from './register/register.component';
import { CreatetaskComponent } from './task/createtask/createtask.component';
import { TaskComponent } from './task/task.component';
import { CreateuserComponent } from './user/createuser/createuser.component';
import { UserComponent } from './user/user.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'main', component: MainComponent,
    children: [
      {
        path: 'user',
        component: UserComponent,
      },
      {
        path: 'task',
        component: TaskComponent,
      },
      {
        path: 'createuser',
        component: CreateuserComponent,
      },
      {
        path: 'createtask',
        component: CreatetaskComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
